class Rate < ActiveRecord::Base
  attr_accessible :ip_address, :product_id, :user_id, :vote
  belongs_to :user
  belongs_to :product 
end
