class Product < ActiveRecord::Base
  attr_accessible :name
  has_many :rates
  
  def average_rating
  	users = self.rates.count
	 	users = users != 0 ? users : 1.0
	 	average = self.rates.sum(:vote) / users
  	average.round(2)
  end
  
  def rate_by_user(user,vote)
		rate = self.rates.where("user_id = ? ",user.id).first
		rate = rate.present? ? rate : self.rates.create(:user_id=>user.id)
		rate.vote = vote
		rate.save
		rate
  end
  
  def rate_by_ip(ip,vote)
	  rate = self.rates.where("ip_address like ? ",ip).first  	
		rate = rate.present? ? rate : self.rates.create(:ip_address=>ip)
		rate.vote = vote
		rate.save
		rate
	end
end
