class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.integer :product_id
      t.integer :user_id
      t.string :ip_address
      t.float :vote

      t.timestamps
    end
  end
end
